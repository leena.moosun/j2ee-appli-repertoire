package forms;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.leena.beans.Contact;

public class CreationContactForm {
    private static final String CHAMP_NOM = "nom";
    private static final String CHAMP_PRENOM = "prenom";
    private static final String CHAMP_TELEPHONE = "telephone";
    private Map<String, String> erreurs = new HashMap<String, String>();
    private String resultat;
    
    public Map<String, String> getErreurs() {
		return erreurs;
	}

	public String getResultat() {
		return resultat;
	}

	public Contact creerContact(HttpServletRequest request){
    	String nom = request.getParameter(CHAMP_NOM);
        String prenom = request.getParameter(CHAMP_PRENOM);
        String telephone = request.getParameter(CHAMP_TELEPHONE);

    	Contact save = new Contact();
    	
        try {
            validationNom( nom );
            save.setNom(nom);
        } catch ( Exception e ) {
            erreurs.put( CHAMP_NOM, e.getMessage() );
        }
        
        try {
            validationPrenom( prenom );
            save.setPrenom(prenom);
        } catch ( Exception e ) {
            erreurs.put( CHAMP_PRENOM, e.getMessage() );
        }

        try {
            validationTelephone( telephone );
            save.setTelephone(telephone);
        } catch ( Exception e ) {
            erreurs.put( CHAMP_TELEPHONE, e.getMessage() );
        }
        
        if( ! erreurs.isEmpty()) {
        	resultat = "Contact non cr��.";
        	return null;
        }

    	resultat = "Contact cr��.";
        return save;
        
    }
    
    private void validationTelephone( String telephone ) throws Exception{
		if(telephone == null || telephone == "") {
        	throw new Exception("Telephone incorrect.");
        }
	}
	
	private void validationPrenom( String prenom ) throws Exception{
		if(prenom == null || prenom == "") {
        	throw new Exception("Prenom incorrect.");
        }
	}
	
	private void validationNom( String nom ) throws Exception{
		if(nom == null || nom == "") {
        	throw new Exception("Nom incorrect.");
        }
	}

}
