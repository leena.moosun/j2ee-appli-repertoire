package com.leena.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.leena.beans.Contact;

import forms.CreationContactForm;

// CONTROLLEUR

public class CreationContactServlet extends HttpServlet {
    public static final String VUE = "/WEB-INF/nouveauContactForm.jsp";
    public static final String CONTACT = "contact";
    public static final String RESULTAT = "resultat";
    public static final String ERREURS = "erreurs";
    
    public void doGet( HttpServletRequest request, HttpServletResponse response )	throws ServletException, IOException {
        this.getServletContext().getRequestDispatcher(VUE).forward( request, response );
    }
    
	public void doPost( HttpServletRequest request, HttpServletResponse response )
		throws ServletException, IOException {

        CreationContactForm form = new CreationContactForm();
        Contact contact = form.creerContact( request );

		request.setAttribute( RESULTAT, form.getResultat());
		request.setAttribute( CONTACT, contact );
		request.setAttribute( ERREURS, form.getErreurs() );
        
		this.doGet(request, response);
	}

}
