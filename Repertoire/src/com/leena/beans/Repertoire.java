package com.leena.beans;

import java.util.ArrayList;
import java.util.List;

public class Repertoire {
	private Repertoire instance;
	private List<Contact> contacts;
	
	private Repertoire(){
		contacts = new ArrayList<Contact>();
	}
	
	public Repertoire getInstance(){
		if(instance == null ) {
			instance = new Repertoire();
		}
		return instance;
	}
	
}
