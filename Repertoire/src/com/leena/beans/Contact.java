package com.leena.beans;

public class Contact {
	private String nom;
	private String prenom;
	private String telephone;
	
	public String getNom() {
		return nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
}
