<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Repertoire</title>
</head>
<body>

    <c:import url="/inc/menu.jsp" />
    
	<h1>Ajouter un nouveau contact</h1>
	
	<form method="post" action="creerContact">
		<label for="nom">Nom <span class="requis">*</span></label>
		<input type="text" id="nom" name="nom" value="" size="20" maxlength="20" />
		<span class="erreur">${erreurs['nom']}</span>
                <br />
		<label for="prenom">Prénom <span class="requis">*</span></label>
		<input type="text" id="prenom" name="prenom" value="" size="20" maxlength="20" />
		<span class="erreur">${erreurs['prenom']}</span>
                <br />
        <label for="telephone">Téléphone <span class="requis">*</span></label>
		<input type="text" id="telephone" name="telephone" value="" size="20" maxlength="20" />
		<span class="erreur">${erreurs['telephone']}</span>
                <br />
       	<input type="submit" value="Valider"/>
	</form>
	
	<hr>
	
	<h2>Une fois ces informations envoyées, le serveur retourne :</h2>
	
	<h3>Erreurs :</h3>
	<ul>
		<c:forEach var="erreur" items="${ erreurs }" >
			<li><c:out value="${ erreur }" /><br/></li>
		</c:forEach>
	</ul>
	
	<h3>Resultat</h3>
	<p>${ resultat }</p>
	
	<c:if test="${ contact != null }">
		<h3>Contact :</h3>
        <p>Nom : ${ contact.nom }</p>
        <p>Prénom : ${ contact.prenom }</p>
        <p>Telephone : ${ contact.telephone }</p>
	</c:if>
</body>
</html>